package ru.omsu;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class task3 {

/*поиск символьных ссылок. Требуется найти и посчитать символьные ссылки, совпадающие с путем директории,
из которой запускается код
Дополнение: ссылка должна содержать 'link', тогда считаем
Дополнение: проблема с последним / -- решено

запуск из командной строки без праметров
*/

    public static void main(String[] args) throws IOException {

        String userDir = System.getProperty ( "user.dir" );
        File path = new File ( userDir );
        System.out.println ( "path = " + path );
        System.out.println ( " " );
        File parentFile = path.getParentFile ( );
        System.out.println ( );
        int result =0;

        while (parentFile != null) {
            System.out.println ( "parent " + parentFile);
            System.out.println ( " " );

            File[] chieldFiles = parentFile.listFiles ( );
            for ( File chieldFile : chieldFiles ) {
                Path chieldPath = Paths.get ( chieldFile.toURI ( ) );
                if (( Files.isSymbolicLink ( chieldPath ) )) {

                    String absolutePath = chieldFile.getAbsolutePath ( );
                    boolean link = false;

                    absolutePath = absolutePath.toUpperCase();

                    if (absolutePath.length ( ) > 0 && absolutePath.charAt ( absolutePath.length ( ) - 1 ) == '/') {
                        absolutePath = absolutePath.substring(0, absolutePath.length() - 1);
                    }
                    String[] absolArr = absolutePath.split ( "/" );
					
					
                    if (absolArr[absolArr.length - 1].contains ( "LINK" )){
                        link = true;
                    }

                    System.out.println ( "symLink " + chieldFile.getAbsolutePath ( ) );
                    String t = Files.readSymbolicLink ( chieldFile.toPath ( ) ).toString ( );
                    if (t != null && t.length() > 0 && t.charAt(t.length() - 1) == '/') {
                        t = t.substring(0, t.length() - 1);
                    }
                    System.out.println ( t );
                    boolean isMyPath = t.equals ( userDir );
                    System.out.println ( isMyPath );
                    if (isMyPath && link) {
                        result++;
                    }
                }
            }

            parentFile = parentFile.getParentFile ( );

        }
        System.out.println ( result );
    }
}

