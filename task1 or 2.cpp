#include <cstdio>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

int main(int argc, char* argv[])
{
size_t sizeB = atoi(argv[1]);
char buff[sizeB];
int n;

while (n=read(0, buff, sizeB)>0) {
write(1, buff, n);
}

return 0;

}
