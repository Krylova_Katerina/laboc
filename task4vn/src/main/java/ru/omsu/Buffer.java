package ru.omsu;

public class Buffer {
    private StringBuffer buffer;
    private boolean[] forReaders;

    public  Buffer(StringBuffer buffer, boolean[] forReaders) {
        this.buffer = buffer;
        this.forReaders = forReaders;
    }

    public synchronized StringBuffer getBuffer() {
        synchronized (this.buffer){return buffer;}
    }

    public synchronized void setBuffer(StringBuffer buffer) {
        synchronized (this.buffer){this.buffer = buffer;}
    }

    public synchronized boolean[] getForReaders() {
        synchronized (this.forReaders){return forReaders;}
    }

    public synchronized   void setForReaders(boolean[] forReaders) {
        synchronized (this.forReaders){this.forReaders = forReaders;}
    }

    public synchronized void setForReadersI(int i, boolean flag) {
     synchronized (this.forReaders)  { this.forReaders[i] = flag;}
    }
}
