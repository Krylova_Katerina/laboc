package ru.omsu;

import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class Writer extends Thread {
    private Semaphore semaphoreWR;
    private Semaphore semaphoreRD;
    private Buffer buffer;
    private int countReaders;


    public Writer(Semaphore semaphore, Semaphore semaphore1, Buffer buffer, int countReaders) {
        this.semaphoreWR = semaphore;
        this.semaphoreRD = semaphore1;
        this.buffer = buffer;
        this.countReaders = countReaders;

    }

    @Override
    public void run() {

        Scanner in = new Scanner(System.in);
        boolean[] forReaders = new boolean[countReaders];
        Boolean flag = false;
        while (!flag) {
            try {
                semaphoreWR.acquire(countReaders);
                System.out.println("Писатель готов");
                System.out.println("Пиши");
                String read = in.nextLine();
                buffer.setBuffer(buffer.getBuffer().delete(0, buffer.getBuffer().length()));
                buffer.setBuffer(buffer.getBuffer().append(read));


                for (int i=0; i<forReaders.length;i++){
                    forReaders[i]=false;
                }
                buffer.setForReaders(forReaders);

                semaphoreRD.release(countReaders);

                if (read.equals("quit")) {
                    System.out.println("Пока, писатель");
                    flag = true;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

