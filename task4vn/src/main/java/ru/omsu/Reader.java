package ru.omsu;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Reader extends Thread {
    private FileWriter fileWriter;
    private Buffer buffer;
    private Semaphore semaphoreWR;
    private Semaphore semaphoreRD;
    private ReentrantReadWriteLock bufLock = new ReentrantReadWriteLock();
    private ReentrantReadWriteLock fileLock = new ReentrantReadWriteLock();
    private int number;

    public Reader(int num, FileWriter fileWriter, Buffer buffer, Semaphore semaphoreRD, Semaphore semaphoreWR) {
        this.number=num;
        this.fileWriter = fileWriter;
        this.buffer = buffer;
        this.semaphoreWR = semaphoreWR;
        this.semaphoreRD = semaphoreRD;
    }

    @Override
    public void run() {

        while (true) {
            try {
                semaphoreRD.acquire();
                bufLock.readLock().lock();
                if (buffer.getForReaders()[number] == false){

                System.out.println("Читатель #" + number + " готов ");

                if (buffer.getBuffer().indexOf("quit") >= 0) {
                    break;
                }

                System.out.println("(#" + number + ") Читаю " + buffer.getBuffer().substring(0));
                fileLock.writeLock().lock();
                try {
                    fileWriter.write(buffer.getBuffer().substring(0));
                    fileWriter.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    fileLock.writeLock().unlock();

                }
                buffer.setForReadersI(number, true);
            }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                bufLock.readLock().lock();
                semaphoreWR.release();
            }

        }

        System.out.println("Пока, читатель #"+number);

    }

}
