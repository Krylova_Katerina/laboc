package ru.omsu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class Main {
    /*один писатель, много читателей. Писатель читает с клавиатуры, пишет в буфер, читатели ждут. Писатель ждет, пока каждый
     * читатель ровно 1 раз прочитает из буфера и запишет в файл
      * запуск из командной строки
      * вводим 2 параметра
      * количество читателей
      * путь к конечному файлу*/
    public static void main(String[] args) throws MyExeption {
       int count = 0;

        if (args.length == 0) {
            throw new MyExeption("Введи хоть что-то");
        }
        if (args.length == 1) {
            throw new MyExeption("Введи количество читателей/путь к файлу");
        }
        if (args.length == 2) {
            System.out.println("Молодец");

            try {
                count = Integer.parseInt(args[1]);
                if (count < 1) {
                    throw new Exception();
                }
            } catch (Exception ex) {
                throw new MyExeption("Неверно ввёл количество читателей");
            }

        }
        if (args.length > 2) {
            throw new MyExeption("Слишком много. Убери");
        }


        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(new File(args[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }


   /*   FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(new File("ogo.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int count =4;*/

        boolean[] forReaders = new boolean[count];
        for (int i=0; i<forReaders.length;i++){
            forReaders[i]=false;
        }
        StringBuffer bufferSTR = new StringBuffer();
        Buffer buffer = new Buffer(bufferSTR,forReaders);


        Semaphore canWrite = new Semaphore(count);
        Semaphore canRead = new Semaphore(0);


        Writer writer = new Writer(canWrite, canRead, buffer, count);
        writer.start();


        for (int i = 0; i < count; i++) {
            Reader reader = new Reader(i,fileWriter, buffer, canRead, canWrite);
            reader.start();
        }


    }
}
