#include <iostream>

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[])
{
int size = atoi(argv[1]);
char buff[size];
const size_t nBuff = sizeof(buff)/sizeof(*buff);
size_t n;

while((n = fread(buff, 1, nBuff, stdin))>0){
fwrite(buff, n, 1 , stdout);
}

printf("Bufsize = %i", size);
return EXIT_SUCCESS;
    exit(0);
}

